# My Calculator app

This project is a calculator application implemented using Cypress, HTML, CSS, and JavaScript. It allows users to perform basic arithmetic operations and includes automated tests written with Cypress.

## User Manual

1. Clone the repository: `git clone https://github.com/brankabjelica/my_calculator.git`
2. Navigate to the project directory: `cd my_calculator`
3. Open the `index.html` file in your preferred web browser.
4. Start using the calculator by clicking on the buttons for numbers and operations.

## Running Tests with Cypress

To run the automated tests using Cypress, follow these steps:

1. Ensure you have Node.js and npm installed on your machine.
2. Install project dependencies: `npm install`
3. Open Cypress test runner: `npx cypress open`
4. Click on the test file `calculator.test.cy.js` to run the tests in the Cypress UI.
5. Alternatively, you can run the tests in headless mode using the command:
   ```
   npm test
   ```
   or
   ```
   npx cypress run
   ```
   The test results will be automatically generated in the `mochawesome-report` directory. To view the test report, open the HTML report file located in the `mochawesome-report` folder. Additionally, if you want to review the recorded videos of the test runs, you can find them in the `cypress/videos` folder within the `mochawesome-report` directory.

   If you prefer to run the tests without generating a report, use:
   ```
   npm run test:noreport
   ```

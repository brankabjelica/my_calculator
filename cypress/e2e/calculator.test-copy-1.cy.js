describe('Test calculator', () => {
  beforeEach('Open page', ()=>{
    cy.visit('../../index.html')
  })
  it('Verify sum of two and two', () => {
    cy.get('#two').click()
    cy.get('#add').click()
    cy.get('#two').click()
    cy.get('#equal').click()

    cy.get('#display').invoke('val').should('contain', '4')
  })
  it('Verify divide of two and two', () => {
    cy.get('#two').click()
    cy.get('#divide').click()
    cy.get('#two').click()
    cy.get('#equal').click()

    cy.get('#display').invoke('val').should('contain', '1')
  })

  it('Verify multiply of two and two', () => {
    cy.get('#two').click()
    cy.get('#multiply').click()
    cy.get('#two').click()
    cy.get('#equal').click()

    cy.get('#display').invoke('val').should('contain', '4')
  })

  it('Verify subtract of two and two', () => {
    cy.get('#two').click()
    cy.get('#subtract').click()
    cy.get('#two').click()
    cy.get('#equal').click()

    cy.get('#display').invoke('val').should('be.eq', '0')
  })

})